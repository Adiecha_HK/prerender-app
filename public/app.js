var app = angular.module('prerender-angular', ['ngRoute'])
    .config(function($routeProvider, $locationProvider){
        $routeProvider.when('/', {
            templateUrl : 'views/homeView.html',
            controller: 'homeController'
        })

        $routeProvider.when('/about', {
            templateUrl : '/views/aboutView.html',
            controller: 'aboutController'
        })

	    $routeProvider.when('/features', {
	        templateUrl : '/views/featuresView.html',
	        controller : 'featuresController'
	    })

        $routeProvider.otherwise({
                redirectTo : '/'
        });

    	$locationProvider.html5Mode(true);
    	$locationProvider.hashPrefix('!');
    });

function mainController($scope) {
    // We will create an seo variable on the scope and decide which fields we want to populate
    $scope.seo = {
        pageTitle : '',
        pageDescription : ''
    };
}

function homeController($scope) {
    // For this tutorial, we will simply access the $scope.seo variable from the main controller and fill it with content.
    // Additionally you can create a service to update the SEO variables - but that's for another tutorial.
    $scope.$parent.seo = {
        pageTitle : 'AngularJS With Prerender',
        pageDescripton: 'Meta data / description.'
    };
}

function aboutController($scope) {
    $scope.$parent.seo = {
        pageTitle : 'About',
        pageDescripton: 'Meta data / description.'
    };
}

function featuresController($scope) {
    $scope.$parent.seo = {
        pageTitle : 'Features',
        pageDescripton: 'Meta data / description.'
    };
}
